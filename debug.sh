#!/bin/bash

# Debugging script for Wingbits installation

# Function to display help/usage
function display_help {
    echo "Usage: $0 [--log <logfile> | --help]"
    echo "  Optional:"
    echo "  --log <logfile>   Specify the log file (otherwise defaults to most recent in home dir)."
    echo "  --help            Display this help message."
    exit 1
}

# Function to check installed version
check_version() {
    local version
    version_file="/etc/wingbits/version"
    if [[ -e $version_file ]]; then
        read -r version < $version_file
        echo -e "\033[0;32m✓\033[0m Version installed: $version"
    else
        echo -e "\033[0;31m✗\033[0m No file version found"
    fi
}

# Function to check for std formatting in DEVICE_ID
check_device_id() {
    local device_id
    if [[ -f /etc/wingbits/device ]]; then
        read -r device_id < /etc/wingbits/device
	if [[ $device_id =~ ^[a-z]+-[a-z]+-[a-z]+$ ]]; then
            echo -e "\033[0;32m✓\033[0m DEVICE_ID properly formatted. DEVICE_ID used: $device_id"
        else
            echo -e "\033[0;31m✗\033[0m DEVICE_ID is not properly formatted. DEVICE_ID used: $device_id"
        fi
    else
        echo -e "\033[0;31m✗\033[0m No DEVICE_ID file found."
    fi
}

# Function to check if ADSB adapter is plugged in
check_adsb_adapter() {
    local rtl_device=$(lsusb | grep -i "RTL28")
    if [[ -n "$rtl_device" ]]; then
        echo -e "\033[0;32m✓\033[0m ADSB adapter found: $rtl_device"
    else
        echo -e "\033[0;31m✗\033[0m No ADSB adapter (RTL28xx) found."
    fi
}

# Function to check for errors in logs
check_installation_errors() {
    if [[ -f $logfile ]]; then
        local found_error=false

        if grep -w "Error" $logfile; then
            echo -e "\033[0;31m✗\033[0m Installation errors found:"
            grep -w "Error" $logfile
            found_error=true
        fi

        if grep -q "rtlsdr: no supported devices found" $logfile; then
            echo -e "\033[0;31m✗\033[0m No supported ADSB devices found."
            found_error=true
        fi

        if grep -q "You are using the piaware image, this setup script would mess up the configuration" $logfile; then
            echo -e "\033[0;31m✗\033[0m Detected Piaware image: Setup script was stopped to avoid messing up the configuration."
            found_error=true
        fi

        if [ "$found_error" = false ]; then
            echo -e "\033[0;32m✓\033[0m No significant errors found in $logfile"
        fi
    else
	echo -e "\033[0;31m✗\033[0m Install log file not found at '$logfile', skipping checks."
    fi
}

# Function to check if readsb was installed properly
check_readsb_installation() {
    if [ -x "$(command -v readsb)" ]; then
        echo -e "\033[0;32m✓\033[0m readsb is installed properly."
    else
        echo -e "\033[0;31m✗\033[0m readsb is not installed."
    fi
}

# Function to check if vector was installed properly
check_vector_installation() {
    if dpkg -l | grep -qw "vector"; then
        echo -e "\033[0;32m✓\033[0m vector is installed properly."
    else
        echo -e "\033[0;31m✗\033[0m vector is not installed."
    fi
}

# Function to check service status
check_service_status() {
    local services=("vector" "readsb")
    for service in "${services[@]}"; do
        status=$(systemctl is-active "$service")
        if [ "$status" = "active" ]; then
            echo -e "\033[0;32m✓\033[0m $service service is active and running."
        else
            echo -e "\033[0;31m✗\033[0m $service service is not active."
        fi
    done
}

# Function to validate coordinates
check_coordinates() {   
    readsb_file="/etc/default/readsb"
    latitude="none"
    longitude="none"
    local lat_regex='^[-+]?([0-9]|[1-8][0-9])(\.[0-9]+)?$'
    local lon_regex='^[-+]?([0-9]|[1-9][0-9]|1[0-7][0-9]|180)(\.[0-9]+)?$'
    
    if [[ -e $readsb_file ]]; then
        latitude=$(grep -Po -- '--lat \K-?\d+\.\d+' "$readsb_file")
        longitude=$(grep -Po -- '--lon \K-?\d+\.\d+' "$readsb_file")

        # Validate latitude from -90 to +90
        if [[ $latitude =~ $lat_regex && $(awk -v lat="$latitude" 'BEGIN {if (lat >= -90 && lat <= 90) print 1; else print 0}') -eq 1 ]]; then
            echo -e "\033[0;32m✓\033[0m Valid coordinate for latitude: $latitude"
        else
            echo -e "\033[0;31m✗\033[0m Invalid coordinate for latitude: $latitude"
        fi

        # Validate longitude from -180 to +180
        if [[ $longitude =~ $lon_regex && $(awk -v lon="$longitude" 'BEGIN {if (lon >= -180 && lon <= 180) print 1; else print 0}') -eq 1 ]]; then
            echo -e "\033[0;32m✓\033[0m Valid coordinate for longitude: $longitude"
        else
            echo -e "\033[0;31m✗\033[0m Invalid coordinate for longitude: $longitude"
        fi
    else
        echo -e "\033[0;31m✗\033[0m No readsb file found."
    fi
}

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --log)
            shift
            logfile="$1"
            ;;
        --help)
            display_help
            ;;
        *)
            # Ignore other arguments
            ;;
    esac
    shift
done

## Setup logfile path
# If --log is not used, find the last created 'wingbits*.log' file in home directory
if [ -z "$logfile" ]; then
    user_dir=$(getent passwd ${SUDO_USER:-$USER} | cut -d: -f6)
    latest_file=$(ls -rt $user_dir/wingbits_*.log 2>/dev/null | tail -1)
    if [ -n "$latest_file" ]; then
        logfile="$latest_file"
    else
        echo "Error: No matching log file found, please check your $user_dir"
    fi
fi

# Main execution
echo "Starting Wingbits Installation Debugging..."

check_version
check_device_id
check_adsb_adapter
check_installation_errors
check_readsb_installation
check_vector_installation
check_service_status
check_coordinates

echo "Debugging completed."
