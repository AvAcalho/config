# Wingbits

- Register Your Antenna
- Note down the Antenna ID generated during registration.

## Install from binaries


### Set Up Your Antenna:

- SSH into your device.
- Run this command: 
```sh
curl -sL https://gitlab.com/wingbits/config/-/raw/master/download.sh | sudo bash
```
- The script will prompt you to enter the Antenna ID. Use the ID you noted earlier.

-   After the script finishes, restart your device.

### Set Up Your Location for ADS-B tools:

- SSH into your device.
- Using the same coordinates (lat/lon) as when registering the antenna, run this command (replace <lat> with your latitude  and <lon> with your longitude):
```sh
sudo readsb-set-location <lat> <lon>
```

### Open tar1090 interface:

- <http://localhost/tar1090> to access the tar1090 web interface.

## Install with docker-compose

This installation makes use of 2 different containers:
- widehopf ultrafeeder ghcr.io/sdr-enthusiasts/docker-adsb-ultrafeeder (https://github.com/wiedehopf/docker-adsb-ultrafeeder/)
    - Refer to his `README.md` file for more details on all the configurations/customizations that can be made
- vector https://hub.docker.com/r/timberio/vector (https://github.com/timberio/vector/tree/master/distribution/docker)


For docker-compose version you only need 3 files `docker-compose.yml` `vector.yaml` and `.env` from this repository. You can clone the full repository or just download the mentioned files.

The purpose of `.env` file is to hold some configurations that can be adjusted by editing the file and restarting ultrafeeder container. Like Wingbits antenna ID and antenna gain.

### Mandatory Parameters

The following parameters must be set (mandatory) inside `.env` file for the container to function:

| Environment Variable   | Purpose                                                                                                        | Default |
| ---------------------- | -------------------------------------------------------------------------------------------------------------- | ------- |
| `WINGBITS_ID`           | The 3 words code of your antenna (copy from wingbits.com dashboard)
| `FEEDER_TZ`             | Your local timezone in [TZ-database-name](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) format                                              |         |
| `ADSB_SDR_GAIN`         | The gain you want to set (0.0 0.9 1.4 2.7 3.7 7.7 8.7 12.5 14.4 15.7 16.6 19.7 20.7 22.9 25.4 28.0 29.7 32.8 33.8 36.4 37.2 38.6 40.2 42.1 43.4 43.9 44.5 48.0 49.6)
| `FEEDER_LAT`            | The latitude of your antenna |         |
| `FEEDER_LONG`           | The longitude of your antenna |         |

### Optional Parameters

The following parameters must be set (optional) inside `.env` file for the container to function:

| Environment Variable   | Purpose                                                                                                        | Default |
| ---------------------- | -------------------------------------------------------------------------------------------------------------- | ------- |
| `ADSB_SDR_SERIAL`      | Define SRD serial to be used in the case that multiple dongles exist on the system                             |  Unset  |
| `ADSB_SDR_PPM`         | Define SRD dongle PPM offset                                                                                   |  Unset  |

### Web Pages

By default this container related webpages will be exposed on port `8080` if you want to modify it, edit `docker-compose.yml` file and change `8080:80` to `<PORT>:80`
If you have configured the container as described above, you should be able to browse to the following web pages:

- <http://localhost:8080/> to access the tar1090 web interface.
- <http://localhost:8080/?replay> to see a replay of past data
- <http://localhost:8080/?heatmap> to see the heatmap for the past 24 hours
- <http://localhost:8080/?heatmap&realHeat> to see a different heatmap for the past 24 hours
- <http://localhost:8080/?pTracks> to see the tracks of all planes for the past 24 hours
- <http://localhost:8080/graphs1090/> to see performance graphs

If you change the port from `8080` to something else you need to add `:<PORT>` after `dockerhost`

Eg.: If you use `9090:80` browse <http://localhost:9090/> to access the tar1090 web interface or <http://localhost:9090/graphs1090/> to see performance graphs

Note that all endpoints start with http and not https!

### Run it

To run the containers make sure you are on the same folder where you have the files `docker-compose.yml` `vector,yaml` and `.env` and run 
```sh
docker-compose up -d
```

### Restart

To restart ultrafeeder container, tipically after you change some settings on `.env` or `docker-compose.yml` files run the following command:
```sh
docker restart ultrafeeder
```
To restart vector container run the following command:
```sh
docker restart vector
```

### Check logs

To restart check the logs from ultrafeeder or vector containers run one of the following commands:

    docker logs <ultrafeeder|vector>                - to show all logs
    docker logs <ultrafeeder|vector> -f             - to show all logs and keep following
    docker logs <ultrafeeder|vector> --since=1m -f  - to show logs from past 1 minute and keep following

Eg.:
 command:
```sh   
docker logs ultrafeeder -f
```
Or refer to docker documentation one more options.