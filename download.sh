#!/bin/bash

# official install script for wingbits - created/managed by wingbits team
WINGBITS_CONFIG_VERSION="0.0.2"
# Additional contributor shout out to: BigMaxi

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root to set up the service correctly"
    echo "Run it like this:"
    echo "sudo ./download.sh"
    exit 1
fi

current_datetime=$(date +'%Y-%m-%d_%H-%M')
#change to user's home dir
user_dir=$(getent passwd ${SUDO_USER:-$USER} | cut -d: -f6)
cd $user_dir
logfile="${user_dir}/wingbits_${current_datetime}.log"

echo "$(date): Wingbits install start time" >> $logfile

mkdir -p /etc/wingbits

file_device_id="none"

if [[ -e /etc/wingbits/device ]]; then
    read -r file_device_id < /etc/wingbits/device
fi

# Rename existing Vector yaml and toml files to avoid issue with Vector UG
if [[ -e /etc/vector/vector.yaml ]]; then
    mv /etc/vector/vector.yaml /etc/vector/vector.yaml${current_datetime}
fi
if [[ -e /etc/vector/vector.toml ]]; then
    mv /etc/vector/vector.toml /etc/vector/vector.toml${current_datetime}
fi

# Remove Device ID file to prevent issue with vector upgrade or install
rm -f /etc/default/vector

# Function to validate the input format
validate_deviceid() {
    if [[ "$1" =~ ^[a-z]+-[a-z]+-[a-z]+$ ]]; then
        return 0
    else
        echo "Invalid format. Please copy the ID from the antennas dashboard page (three lowercase words separated by hyphens with no spaces)."
        return 1
    fi
}

# Initialize a flag variable
read_from_file=false

# Process command-line arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        --read-from-file)
            read_from_file=true
            shift # Remove the argument from the list
            ;;
        *) # Handle other arguments if necessary
            shift
            ;;
    esac
done

# For debugging purposes, print the values of the variables
#echo "read_from_file: $read_from_file"
#echo "File exists: $( [[ -e /etc/wingbits/device ]] && echo "yes" || echo "no" )"

# get device id from file
if [[ -e /etc/wingbits/device ]] && [[ "$read_from_file" == "true" ]]; then
    read -r device_id < /etc/wingbits/device
else
    while true; do
        read -p "Enter the device ID from antennas page (default: $file_device_id): " device_id </dev/tty

        # Use the default value if the user presses enter without typing anything
        device_id=${device_id:-$file_device_id}

        # Validate the device id
        if validate_deviceid "$device_id"; then
            break
        fi
    done
fi

echo "$device_id" > /etc/wingbits/device
echo "Using device ID: $device_id"
echo "Device ID saved to local config file /etc/wingbits/device"


# Function to display loading animation with an airplane icon
function show_loading() {
  local text=$1
  local delay=0.2
  local frames=("⣾" "⣽" "⣻" "⢿" "⡿" "⣟" "⣯" "⣷")
  local frame_count=${#frames[@]}
  local i=0

  while true; do
    local frame_index=$((i % frame_count))
    printf "\r%s  %s" "${frames[frame_index]}" "${text}"
    sleep $delay
    i=$((i + 1))
  done
}

# Function to run multiple commands and log the output
function run_command() {
  local commands=("$@")
  local text=${commands[0]}
  local command
  echo "===================${text}====================" >> $logfile

  for command in "${commands[@]:1}"; do
    (
      eval "${command}" >> $logfile 2>&1
      printf "done" > /tmp/wingbits.done
    ) &
    local pid=$!

    show_loading "${text}" &
    local spinner_pid=$!

    # Wait for the command to finish
    wait "${pid}"

    # Kill the spinner
    kill "${spinner_pid}"
    wait "${spinner_pid}" 2>/dev/null

    # Check if the command completed successfully
    if [[ -f /tmp/wingbits.done ]]; then
      rm /tmp/wingbits.done
      printf "\r\033[0;32m✓\033[0m   %s\n" "${text}"
    else
      printf "\r\033[0;31m✗\033[0m   %s\n" "${text}"
    fi
  done
}

function check_service_status(){
  local services=("$@")
  for service in "${services[@]}"; do
    status="$(systemctl is-active "$service".service)"
    if [ "$status" != "active" ]; then
        echo "$service is inactive. Waiting 30 seconds..."
        sleep 30 # on initial decoder install (readsb) a reboot is required, but should start fine on updates/reinstalls
        status="$(systemctl is-active "$service".service)"
        if [ "$status" != "active" ]; then
            echo "$service is still inactive."
        else
            echo "$service is now active. ✈"
        fi
    else
        echo "$service is active. ✈"
    fi
  done
}


# Step 1: Update package repositories
run_command "Updating package repositories" "apt-get update"

# Step 2: Upgrade installed packages
run_command "Upgrading installed packages" "apt-get upgrade -y"

# Step 3: Install curl if not already installed
run_command "Installing curl" "apt-get -y install curl"

# Step 4: Download and install readsb
run_command "Installing readsb" \
  "curl -sL https://github.com/wiedehopf/adsb-scripts/raw/master/readsb-install.sh | bash" \
	"sed -i -e 's|After=.*|After=vector.service|' /lib/systemd/system/readsb.service" \
	"curl -sL https://github.com/wiedehopf/graphs1090/raw/master/install.sh | bash"
	
if grep -q -- "--net-connector localhost,30006,json_out" /etc/default/readsb ; then
	echo "readsb already configured for Wingbits" | sudo tee >> $logfile
else
	sed -i.bak 's/NET_OPTIONS="[^"]*/& '"--net-connector localhost,30006,json_out"'/' /etc/default/readsb
	echo "Added Wingbits config to readsb config file" | sudo tee >> $logfile
fi


# Step 5: Download and install Vector

#run outside run_command
 CSM_MIGRATE=true bash -c "$(curl -L https://setup.vector.dev)" >> $logfile 2>&1

run_command "Installing vector" \
  "apt-get -y install vector" \
  "mkdir -p /etc/vector" \
  "touch /etc/vector/vector.yaml" \
  "curl -o /etc/vector/vector.yaml '"https://gitlab.com/wingbits/config/-/raw/$WINGBITS_CONFIG_VERSION/vector.yaml"'" \
  "sed -i 's|ExecStart=.*|ExecStart=/usr/bin/vector --watch-config|' /lib/systemd/system/vector.service"

  echo "DEVICE_ID=\"$device_id\"" > /etc/default/vector
  chmod 644 /etc/default/vector

# Step 6: Reload systemd daemon, enable and start services
run_command "Starting services" \
  "systemctl daemon-reload" \
  "systemctl enable vector" \
  "systemctl restart readsb vector"

# Step 7: Create the check status cron job
echo '#!/bin/bash
STATUS="$(systemctl is-active vector.service)"

if [ "$STATUS" != "active" ]; then
    systemctl restart vector.service
    echo "$(date): Service was restarted" >> $logfile
fi' > /etc/wingbits/check_status.sh && \
sudo chmod +x /etc/wingbits/check_status.sh && \
echo "*/5 * * * * root /bin/bash /etc/wingbits/check_status.sh" | sudo tee /etc/cron.d/wingbits

# Step 8: Save the version number

run_command "Saving version number" \
    "echo $WINGBITS_CONFIG_VERSION > /etc/wingbits/version"

# Step 9: Check if services are online
check_service_status "vector" "readsb"

# Step 10: Create a search for updates cron on every 2 hours in the file update.sh

# Variables
SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/$WINGBITS_CONFIG_VERSION/update.sh"
CRON_IDENTIFIER="wingbits_config_update"  # Unique identifier for the cron job
LOG_FILE="/etc/wingbits/logs/update.log"

# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "$CRON_IDENTIFIER"; then
    (crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
fi
# Add the new cron job

UPDATE_JOB="0 */2 * * * /usr/bin/curl -s $SCRIPT_URL | /bin/bash > $LOG_FILE 2>&1 # $CRON_IDENTIFIER"
(crontab -l ; echo "$UPDATE_JOB") | crontab -


echo -e "\n\033[0;32mInstallation completed successfully!\033[0m"

echo -e "\nPlease restart with \"sudo reboot\" if this is your first install of adsb software on this machine and readsb did not show as active above this line!"

echo "$(date): Wingbits install end time" >> $logfile