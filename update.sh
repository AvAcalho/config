#!/bin/bash

local_version=$(cat /etc/wingbits/version)
echo "Current local version: $local_version"

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root to set up the service correctly"
    echo "Run it like this:"
    echo "sudo ./download.sh"
    exit 1
fi

SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/download.sh"
script=$(curl $SCRIPT_URL)
version=$(echo "$script" | grep -oP '(?<=WINGBITS_CONFIG_VERSION=")[^"]*')
echo "Latest available wingbits version: $version"


if [ "$version" != "$local_version" ] || [ -z "$version" ]; then
    echo "Updating wingbits..."
    curl -sL $SCRIPT_URL | sudo bash -c 'bash /dev/stdin --read-from-file'
else
    echo "Wingbits is up to date"
    exit 0
fi
